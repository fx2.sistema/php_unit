<?php

namespace App\Repositories;

use App\Entities\Client;

class ClientRepository
{
    /**
     * @var Client
     */
    protected Client $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function save(int $id, string $name)
    {
        $client = new $this->client();
        $client->setId($id);
        $client->setName($name);

        return ['id' => $id, 'name' => $name];
    }

    public function get()
    {
        return $this->client->listOfClients();
    }

    public function getByName(string $name)
    {
        return array_filter($this->client->listOfClients(), function($client) use ($name) {
            return $client['name'] == $name;
        });
    }
}