<?php

namespace Services;

use App\Entities\Client;
use App\Repositories\ClientRepository;
use App\Services\ClientService;
use PHPUnit\Framework\TestCase;

class ClientServiceTest extends TestCase
{
    public function testSaveClient()
    {
        $client = $this->createMock(Client::class);
        $client->method('listOfClients')->willReturn([
            ['id' => 1, 'name' => 'Fernando'],
            ['id' => 2, 'name' => 'Fran'],
            ['id' => 3, 'name' => 'AM'],
        ]);

        $clientRepository = $this->createMock(ClientRepository::class);
        $clientRepository->method('getByName')->willReturn([]);
        $clientRepository->method('save')->willReturn(['id' => 4, 'name' => 'Guto']);
        $clientService = new ClientService($clientRepository);

        $response = $clientService->saveClient('Guto');

        $this->assertIsArray($response);
        $this->assertEquals(4, $response['id']);
        $this->assertEquals('Guto', $response['name']);
    }

    /**
     * @return void
     * @throws \Exception
     */
    public function testSaveClientWithError()
    {
        $client = $this->createMock(Client::class);
        $client->method('listOfClients')->willReturn([
            ['id' => 1, 'name' => 'Fernando'],
            ['id' => 2, 'name' => 'Fran'],
            ['id' => 3, 'name' => 'AM'],
        ]);

        $clientRepository = $this->createMock(ClientRepository::class);
        $clientRepository->method('getByName')->willReturn(['id' => 1, 'name' => 'Fernando']);
        $clientService = new ClientService($clientRepository);

        $this->expectException('Exception');
        $response = $clientService->saveClient('Fernando');
    }

    /**
     * @group ax
     * @return void
     */
    public function testGetClient()
    {
        $clientRepository = $this->createMock(ClientRepository::class);
        $clientRepository->method('getByName')->willReturn(['id' => 1, 'name' => 'Fernando']);
        $clientService = new ClientService($clientRepository);

        $response = $clientService->getClient('Fernando');

        $this->assertEquals('Fernando', $response['name']);
        $this->assertCount(2, $response);
    }

    public function testGetClientNull()
    {
        $clientRepository = $this->createMock(ClientRepository::class);
        $clientRepository->method('getByName')->willReturn([]);
        $clientService = new ClientService($clientRepository);

        $response = $clientService->getClient('');

        $this->assertEmpty($response);
    }
}