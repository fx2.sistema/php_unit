<?php

namespace Repositories;

use App\Entities\Client;
use App\Repositories\ClientRepository;
use PHPUnit\Framework\TestCase;

class ClientRepositoryTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp(); // TODO: Change the autogenerated stub
        $this->client = $this->createMock(Client::class);
        $this->client->method('listOfClients')->willReturn([
            ['id' => 1, 'name' => 'Fernando'],
            ['id' => 2, 'name' => 'Fran'],
            ['id' => 3, 'name' => 'AM'],
        ]);

        $this->clientRepository = new ClientRepository($this->client);
    }

    /**
     * @return void
     */
    public function testSave()
    {
        $savedClient = $this->clientRepository->save(1, 'Fernando');

        $this->assertEquals(['id' => 1, 'name' => 'Fernando'] , $savedClient);
    }

    public function testGet()
    {
        $getClients = $this->clientRepository->get();

        $this->assertIsArray($getClients);
        $this->assertNotEmpty($getClients);
    }

    public function testGetByNameNotEmpty()
    {
        $getClients = $this->clientRepository->getByName('Fernando');

        $this->assertIsArray($getClients);
        $this->assertNotEmpty($getClients);
    }

    public function testGetByNameEmpty()
    {
        $getClients = $this->clientRepository->getByName('Claudio');

        $this->assertIsArray($getClients);
        $this->assertEmpty($getClients);
    }
}